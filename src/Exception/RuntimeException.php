<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Session
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Plugin\Session\Exception;

/**
 *
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface {

}
